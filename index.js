module.exports = {
  "extends": [
    "standard"
  ],
  
  "rules": {
    "array-bracket-spacing": 2,
    "arrow-parens": [2, "as-needed", { "requireForBlockBody": true }],
    "camelcase": [2, { "properties": "always" }],
    "jsx-quotes": 2,
    "linebreak-style": [2, "unix"],
    "max-len": [2, 120, 2],
    "no-await-in-loop": 2,
    "no-console": [2, { "allow": ["info", "warn", "error"] }],
    "no-var": 2,
    "object-shorthand": 2,
    "prefer-const": 2,
    "prefer-template": 2,
    "quote-props": [2, "as-needed"],
    "space-before-function-paren": [2, { "anonymous": "always", "named": "never" }],
    "strict": [2, "global"],    

    "import/no-unresolved": 2,
    "import/order": 2,
    "global-require": 2,

    "babel/new-cap": 2,
    "babel/object-curly-spacing": [2, "always"],
    
    "no-exclusive-tests": "error"
  },

  "plugins": [
    "babel",
    "import"
  ]
}
